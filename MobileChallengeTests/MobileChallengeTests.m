//
//  MobileChallengeTests.m
//  MobileChallengeTests
//
//  Created by John Ojanen on 2/25/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FNLocationData.h"
#import "FNLocations.h"

@interface MobileChallengeTests : XCTestCase

@property FNLocations *testLocations;

@end

@implementation MobileChallengeTests

- (void)setUp
{
    [super setUp];
    
	self.testLocations = [[FNLocations alloc] initLocations];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
	self.testLocations = nil;
	
    [super tearDown];
}

- (void)testLocationsExist
{
    XCTAssertTrue([self.testLocations locationCount] > 0, @"Locations not initialized correctly!");
}


- (void)testNonEmptyLocations
{
	for (NSUInteger index = 0; index < [self.testLocations locationCount]; index++)
	{
		FNLocationData *testLocData = [self.testLocations locationAtIndex:index];
		XCTAssertTrue(testLocData.name.length > 0, @"Location name is empty!");
		XCTAssertTrue(testLocData.address.length > 0, @"Location address is empty!");
	}
}


- (void)testOutOfRangeHandling
{
	NSUInteger locCount = [self.testLocations locationCount];
	FNLocationData *testLocData = [self.testLocations locationAtIndex:locCount + 1000];
	XCTAssertTrue(testLocData == NULL, @"Location out of range returned object!");
}

@end
