//
//  FNLocationData.h
//  MobileChallenge
//
//  Created by John Ojanen on 2/25/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import <Foundation/Foundation.h>

//________________________________________________________________________________
// object that represents a location

@interface FNLocationData : NSObject

@property (readonly, nonatomic) NSString *name;
@property (readonly, nonatomic) NSString *address;

- (instancetype)initWithName:(NSString *)name address:(NSString *)address;

@end

