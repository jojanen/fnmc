//
//  FNLocationData.m
//  MobileChallenge
//
//  Created by John Ojanen on 2/25/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import "FNLocationData.h"

@implementation FNLocationData


- (instancetype)initWithName:(NSString *)name address:(NSString *)address
{
	self = [super init];
	
	if (self) {
		_name = name;
		_address = address;
	}
	return self;
}


@end


