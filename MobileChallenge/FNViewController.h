//
//  FNViewController.h
//  MobileChallenge
//
//  Created by John Ojanen on 2/25/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FNViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property IBOutlet UITableView *locationsTable;

@end
