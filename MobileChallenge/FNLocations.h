//
//  FNLocations.h
//  MobileChallenge
//
//  Created by John Ojanen on 2/25/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FNLocationData;

//________________________________________________________________________________
// object that holds a list of locations

@interface FNLocations : NSObject

- (instancetype)initLocations;
- (NSUInteger)locationCount;
- (FNLocationData *)locationAtIndex:(NSUInteger)index;

@end
