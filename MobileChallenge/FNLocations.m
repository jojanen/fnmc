//
//  FNLocations.m
//  MobileChallenge
//
//  Created by John Ojanen on 2/25/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import "FNLocations.h"
#import "FNLocationData.h"


//________________________________________________________________________________
//

@interface FNLocations ()

@property NSMutableArray *locationsList;
- (void)populateLocations;

@end


//________________________________________________________________________________
//

@implementation FNLocations


//________________________________________________________________________________
//

- (instancetype)initLocations
{
	self = [super init];
	if (self) {
		self.locationsList = [NSMutableArray arrayWithCapacity:10];
		[self populateLocations];
	}
	return self;
}


//________________________________________________________________________________
//

- (NSUInteger)locationCount
{
	return [self.locationsList count];
}


//________________________________________________________________________________
//

- (FNLocationData *)locationAtIndex:(NSUInteger)index
{
	FNLocationData *loc = NULL;
	if (index < [self locationCount])
		loc = [self.locationsList objectAtIndex:index];
	return loc;
}


//________________________________________________________________________________
//

- (void)populateLocations
{
	NSArray *tempLocs = @[@"Field Nation",
						  @"310 S 4th Ave #8100, Minneapolis, MN 55415",
						  @"Minneapolis Institute of Arts",
						  @"2400 3rd Ave S, Minneapolis, MN 55404",
						  @"Target Center",
						  @"600 First Avenue North, Minneapolis, MN 55303",
						  @"Xcel Energy Center",
						  @"199 Kellogg Blvd W, St Paul, MN 55102",
						  @"Fort Snelling",
						  @"200 Tower Ave, St Paul, MN 55111",
						  @"Mayo Clinic",
						  @"200 1st St SW, Rochester, MN 55905",
						  @"Fitger’s Brewhouse",
						  @"600 E Superior St, Duluth, MN 55802",
						  @"Minnesota State Capitol",
						  @"75 Rev Dr Martin Luther King Jr Boulevard, St Paul, MN 55155",
						  @"Wild River State Park",
						  @"39797 Park Trail, Center City, MN 55012",
						  @"Split Rock Lighthouse",
						  @"3713 Split Rock Lighthouse, Two Harbors, MN 55616"];
	
	for (NSUInteger ix = 0; ix < [tempLocs count]; ix += 2)
	{
		FNLocationData *loc = [[FNLocationData alloc] initWithName:tempLocs[ix] address:tempLocs[ix + 1]];
		[self.locationsList addObject:loc];
	}
}


@end
