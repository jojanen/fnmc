//
//  FNViewController.m
//  MobileChallenge
//
//  Created by John Ojanen on 2/25/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import "FNViewController.h"
#import "FNLocationData.h"
#import "FNLocations.h"

#import <MapKit/MapKit.h>


@interface FNViewController ()

@property FNLocations *locations;

@end

@implementation FNViewController

//________________________________________________________________________________
//

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}


//________________________________________________________________________________
//

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//________________________________________________________________________________
//

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	self.locations = [[FNLocations alloc] initLocations];
}


//________________________________________________________________________________
// UITableViewDataSource methods
// Normally I like to separate the data source code from the view
// controller but this is a very short term project.


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.locations locationCount];
}


//________________________________________________________________________________
//

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *kFNLocationCellID = @"LocationCell42";
	UITableViewCell *locCell = [self.locationsTable dequeueReusableCellWithIdentifier:kFNLocationCellID];
	if (locCell == NULL)
	{
		locCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kFNLocationCellID];
		FNLocationData *dataForCell = [self.locations locationAtIndex:indexPath.row];
		locCell.textLabel.text = dataForCell.name;
		locCell.detailTextLabel.text = dataForCell.address;
		UIView *bgColorView = [[UIView alloc] init];
		bgColorView.backgroundColor = [UIColor blueColor];
		locCell.selectedBackgroundView = bgColorView;
	}

	return locCell;
}


//________________________________________________________________________________
// UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	FNLocationData *dataForCell = [self.locations locationAtIndex:indexPath.row];
	NSString *address = dataForCell.address;

	NSString *urlString = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@", address];
	NSURL *myurl = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	[[UIApplication sharedApplication] openURL:myurl];
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}


//________________________________________________________________________________
// These cell highlight/unhightlight routines are used to swap the text color.
// Setting the cell background color didn't work here because the table enforced
// its highlight color setting over everything else. Got it to work by setting
// the selectedBackgroundView when the cell is made in cellForRowAtIndexPath.

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *hiliteCell = [tableView cellForRowAtIndexPath:indexPath];
	hiliteCell.textLabel.textColor = [UIColor whiteColor];
	hiliteCell.detailTextLabel.textColor = [UIColor whiteColor];
}


- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *hiliteCell = [tableView cellForRowAtIndexPath:indexPath];
	hiliteCell.textLabel.textColor = [UIColor blackColor];
	hiliteCell.detailTextLabel.textColor = [UIColor blackColor];
}


@end
