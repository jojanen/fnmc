//
//  main.m
//  MobileChallenge
//
//  Created by John Ojanen on 2/25/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FNAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([FNAppDelegate class]));
	}
}
